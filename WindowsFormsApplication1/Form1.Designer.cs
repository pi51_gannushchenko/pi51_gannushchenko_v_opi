﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.играToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.режимToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.WithPCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.WithFriendToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.полеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.x3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.х4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.x5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.A1 = new System.Windows.Forms.Button();
            this.A2 = new System.Windows.Forms.Button();
            this.A3 = new System.Windows.Forms.Button();
            this.B1 = new System.Windows.Forms.Button();
            this.B2 = new System.Windows.Forms.Button();
            this.B3 = new System.Windows.Forms.Button();
            this.C1 = new System.Windows.Forms.Button();
            this.C2 = new System.Windows.Forms.Button();
            this.C3 = new System.Windows.Forms.Button();
            this.A4 = new System.Windows.Forms.Button();
            this.B4 = new System.Windows.Forms.Button();
            this.C4 = new System.Windows.Forms.Button();
            this.D1 = new System.Windows.Forms.Button();
            this.D4 = new System.Windows.Forms.Button();
            this.D2 = new System.Windows.Forms.Button();
            this.D3 = new System.Windows.Forms.Button();
            this.E1 = new System.Windows.Forms.Button();
            this.E4 = new System.Windows.Forms.Button();
            this.E2 = new System.Windows.Forms.Button();
            this.E3 = new System.Windows.Forms.Button();
            this.A5 = new System.Windows.Forms.Button();
            this.B5 = new System.Windows.Forms.Button();
            this.C5 = new System.Windows.Forms.Button();
            this.D5 = new System.Windows.Forms.Button();
            this.E5 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.играToolStripMenuItem,
            this.режимToolStripMenuItem,
            this.полеToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(203, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // играToolStripMenuItem
            // 
            this.играToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newGameToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.играToolStripMenuItem.Name = "играToolStripMenuItem";
            this.играToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.играToolStripMenuItem.Text = "Гра";
            // 
            // newGameToolStripMenuItem
            // 
            this.newGameToolStripMenuItem.Name = "newGameToolStripMenuItem";
            this.newGameToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.newGameToolStripMenuItem.Text = "Нова гра";
            this.newGameToolStripMenuItem.Click += new System.EventHandler(this.newGameToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.exitToolStripMenuItem.Text = "Вихід";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // режимToolStripMenuItem
            // 
            this.режимToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.WithPCToolStripMenuItem,
            this.WithFriendToolStripMenuItem});
            this.режимToolStripMenuItem.Name = "режимToolStripMenuItem";
            this.режимToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.режимToolStripMenuItem.Text = "Режим";
            // 
            // WithPCToolStripMenuItem
            // 
            this.WithPCToolStripMenuItem.Name = "WithPCToolStripMenuItem";
            this.WithPCToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.WithPCToolStripMenuItem.Text = "Проти ПК";
            this.WithPCToolStripMenuItem.Click += new System.EventHandler(this.WithPCToolStripMenuItem_Click);
            // 
            // WithFriendToolStripMenuItem
            // 
            this.WithFriendToolStripMenuItem.Name = "WithFriendToolStripMenuItem";
            this.WithFriendToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.WithFriendToolStripMenuItem.Text = "З другом";
            this.WithFriendToolStripMenuItem.Click += new System.EventHandler(this.WithFriendToolStripMenuItem_Click);
            // 
            // полеToolStripMenuItem
            // 
            this.полеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.x3ToolStripMenuItem,
            this.х4ToolStripMenuItem,
            this.x5ToolStripMenuItem});
            this.полеToolStripMenuItem.Name = "полеToolStripMenuItem";
            this.полеToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.полеToolStripMenuItem.Text = "Поле";
            // 
            // x3ToolStripMenuItem
            // 
            this.x3ToolStripMenuItem.Name = "x3ToolStripMenuItem";
            this.x3ToolStripMenuItem.Size = new System.Drawing.Size(91, 22);
            this.x3ToolStripMenuItem.Text = "3x3";
            this.x3ToolStripMenuItem.Click += new System.EventHandler(this.x3ToolStripMenuItem_Click);
            // 
            // х4ToolStripMenuItem
            // 
            this.х4ToolStripMenuItem.Name = "х4ToolStripMenuItem";
            this.х4ToolStripMenuItem.Size = new System.Drawing.Size(91, 22);
            this.х4ToolStripMenuItem.Text = "4х4";
            this.х4ToolStripMenuItem.Click += new System.EventHandler(this.х4ToolStripMenuItem_Click);
            // 
            // x5ToolStripMenuItem
            // 
            this.x5ToolStripMenuItem.Name = "x5ToolStripMenuItem";
            this.x5ToolStripMenuItem.Size = new System.Drawing.Size(91, 22);
            this.x5ToolStripMenuItem.Text = "5x5";
            this.x5ToolStripMenuItem.Click += new System.EventHandler(this.x5ToolStripMenuItem_Click);
            // 
            // A1
            // 
            this.A1.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.A1.Location = new System.Drawing.Point(0, 27);
            this.A1.Name = "A1";
            this.A1.Size = new System.Drawing.Size(63, 55);
            this.A1.TabIndex = 1;
            this.A1.UseVisualStyleBackColor = true;
            this.A1.Click += new System.EventHandler(this.button_click);
            this.A1.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.A1.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // A2
            // 
            this.A2.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.A2.Location = new System.Drawing.Point(69, 27);
            this.A2.Name = "A2";
            this.A2.Size = new System.Drawing.Size(63, 55);
            this.A2.TabIndex = 1;
            this.A2.UseVisualStyleBackColor = true;
            this.A2.Click += new System.EventHandler(this.button_click);
            this.A2.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.A2.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // A3
            // 
            this.A3.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.A3.Location = new System.Drawing.Point(138, 27);
            this.A3.Name = "A3";
            this.A3.Size = new System.Drawing.Size(63, 55);
            this.A3.TabIndex = 1;
            this.A3.UseVisualStyleBackColor = true;
            this.A3.Click += new System.EventHandler(this.button_click);
            this.A3.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.A3.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // B1
            // 
            this.B1.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.B1.Location = new System.Drawing.Point(0, 88);
            this.B1.Name = "B1";
            this.B1.Size = new System.Drawing.Size(63, 55);
            this.B1.TabIndex = 1;
            this.B1.UseVisualStyleBackColor = true;
            this.B1.Click += new System.EventHandler(this.button_click);
            this.B1.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.B1.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // B2
            // 
            this.B2.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.B2.Location = new System.Drawing.Point(69, 88);
            this.B2.Name = "B2";
            this.B2.Size = new System.Drawing.Size(63, 55);
            this.B2.TabIndex = 1;
            this.B2.UseVisualStyleBackColor = true;
            this.B2.Click += new System.EventHandler(this.button_click);
            this.B2.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.B2.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // B3
            // 
            this.B3.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.B3.Location = new System.Drawing.Point(138, 88);
            this.B3.Name = "B3";
            this.B3.Size = new System.Drawing.Size(63, 55);
            this.B3.TabIndex = 1;
            this.B3.UseVisualStyleBackColor = true;
            this.B3.Click += new System.EventHandler(this.button_click);
            this.B3.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.B3.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // C1
            // 
            this.C1.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.C1.Location = new System.Drawing.Point(0, 149);
            this.C1.Name = "C1";
            this.C1.Size = new System.Drawing.Size(63, 55);
            this.C1.TabIndex = 1;
            this.C1.UseVisualStyleBackColor = true;
            this.C1.Click += new System.EventHandler(this.button_click);
            this.C1.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.C1.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // C2
            // 
            this.C2.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.C2.Location = new System.Drawing.Point(69, 149);
            this.C2.Name = "C2";
            this.C2.Size = new System.Drawing.Size(63, 55);
            this.C2.TabIndex = 1;
            this.C2.UseVisualStyleBackColor = true;
            this.C2.Click += new System.EventHandler(this.button_click);
            this.C2.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.C2.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // C3
            // 
            this.C3.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.C3.Location = new System.Drawing.Point(138, 149);
            this.C3.Name = "C3";
            this.C3.Size = new System.Drawing.Size(63, 55);
            this.C3.TabIndex = 1;
            this.C3.UseVisualStyleBackColor = true;
            this.C3.Click += new System.EventHandler(this.button_click);
            this.C3.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.C3.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // A4
            // 
            this.A4.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.A4.Location = new System.Drawing.Point(207, 27);
            this.A4.Name = "A4";
            this.A4.Size = new System.Drawing.Size(63, 55);
            this.A4.TabIndex = 1;
            this.A4.UseVisualStyleBackColor = true;
            this.A4.Click += new System.EventHandler(this.button_click);
            this.A4.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.A4.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // B4
            // 
            this.B4.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.B4.Location = new System.Drawing.Point(207, 88);
            this.B4.Name = "B4";
            this.B4.Size = new System.Drawing.Size(63, 55);
            this.B4.TabIndex = 1;
            this.B4.UseVisualStyleBackColor = true;
            this.B4.Click += new System.EventHandler(this.button_click);
            this.B4.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.B4.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // C4
            // 
            this.C4.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.C4.Location = new System.Drawing.Point(207, 149);
            this.C4.Name = "C4";
            this.C4.Size = new System.Drawing.Size(63, 55);
            this.C4.TabIndex = 1;
            this.C4.UseVisualStyleBackColor = true;
            this.C4.Click += new System.EventHandler(this.button_click);
            this.C4.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.C4.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // D1
            // 
            this.D1.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.D1.Location = new System.Drawing.Point(0, 210);
            this.D1.Name = "D1";
            this.D1.Size = new System.Drawing.Size(63, 55);
            this.D1.TabIndex = 1;
            this.D1.UseVisualStyleBackColor = true;
            this.D1.Click += new System.EventHandler(this.button_click);
            this.D1.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.D1.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // D4
            // 
            this.D4.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.D4.Location = new System.Drawing.Point(207, 210);
            this.D4.Name = "D4";
            this.D4.Size = new System.Drawing.Size(63, 55);
            this.D4.TabIndex = 1;
            this.D4.UseVisualStyleBackColor = true;
            this.D4.Click += new System.EventHandler(this.button_click);
            this.D4.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.D4.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // D2
            // 
            this.D2.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.D2.Location = new System.Drawing.Point(69, 210);
            this.D2.Name = "D2";
            this.D2.Size = new System.Drawing.Size(63, 55);
            this.D2.TabIndex = 1;
            this.D2.UseVisualStyleBackColor = true;
            this.D2.Click += new System.EventHandler(this.button_click);
            this.D2.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.D2.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // D3
            // 
            this.D3.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.D3.Location = new System.Drawing.Point(138, 210);
            this.D3.Name = "D3";
            this.D3.Size = new System.Drawing.Size(63, 55);
            this.D3.TabIndex = 1;
            this.D3.UseVisualStyleBackColor = true;
            this.D3.Click += new System.EventHandler(this.button_click);
            this.D3.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.D3.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // E1
            // 
            this.E1.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.E1.Location = new System.Drawing.Point(0, 269);
            this.E1.Name = "E1";
            this.E1.Size = new System.Drawing.Size(63, 55);
            this.E1.TabIndex = 1;
            this.E1.UseVisualStyleBackColor = true;
            this.E1.Click += new System.EventHandler(this.button_click);
            this.E1.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.E1.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // E4
            // 
            this.E4.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.E4.Location = new System.Drawing.Point(207, 269);
            this.E4.Name = "E4";
            this.E4.Size = new System.Drawing.Size(63, 55);
            this.E4.TabIndex = 1;
            this.E4.UseVisualStyleBackColor = true;
            this.E4.Click += new System.EventHandler(this.button_click);
            this.E4.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.E4.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // E2
            // 
            this.E2.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.E2.Location = new System.Drawing.Point(69, 269);
            this.E2.Name = "E2";
            this.E2.Size = new System.Drawing.Size(63, 55);
            this.E2.TabIndex = 1;
            this.E2.UseVisualStyleBackColor = true;
            this.E2.Click += new System.EventHandler(this.button_click);
            this.E2.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.E2.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // E3
            // 
            this.E3.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.E3.Location = new System.Drawing.Point(138, 269);
            this.E3.Name = "E3";
            this.E3.Size = new System.Drawing.Size(63, 55);
            this.E3.TabIndex = 1;
            this.E3.UseVisualStyleBackColor = true;
            this.E3.Click += new System.EventHandler(this.button_click);
            this.E3.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.E3.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // A5
            // 
            this.A5.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.A5.Location = new System.Drawing.Point(276, 27);
            this.A5.Name = "A5";
            this.A5.Size = new System.Drawing.Size(63, 55);
            this.A5.TabIndex = 1;
            this.A5.UseVisualStyleBackColor = true;
            this.A5.Click += new System.EventHandler(this.button_click);
            this.A5.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.A5.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // B5
            // 
            this.B5.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.B5.Location = new System.Drawing.Point(276, 88);
            this.B5.Name = "B5";
            this.B5.Size = new System.Drawing.Size(63, 55);
            this.B5.TabIndex = 1;
            this.B5.UseVisualStyleBackColor = true;
            this.B5.Click += new System.EventHandler(this.button_click);
            this.B5.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.B5.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // C5
            // 
            this.C5.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.C5.Location = new System.Drawing.Point(276, 149);
            this.C5.Name = "C5";
            this.C5.Size = new System.Drawing.Size(63, 55);
            this.C5.TabIndex = 1;
            this.C5.UseVisualStyleBackColor = true;
            this.C5.Click += new System.EventHandler(this.button_click);
            this.C5.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.C5.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // D5
            // 
            this.D5.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.D5.Location = new System.Drawing.Point(276, 210);
            this.D5.Name = "D5";
            this.D5.Size = new System.Drawing.Size(63, 55);
            this.D5.TabIndex = 1;
            this.D5.UseVisualStyleBackColor = true;
            this.D5.Click += new System.EventHandler(this.button_click);
            this.D5.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.D5.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // E5
            // 
            this.E5.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.E5.Location = new System.Drawing.Point(276, 269);
            this.E5.Name = "E5";
            this.E5.Size = new System.Drawing.Size(63, 55);
            this.E5.TabIndex = 1;
            this.E5.UseVisualStyleBackColor = true;
            this.E5.Click += new System.EventHandler(this.button_click);
            this.E5.MouseEnter += new System.EventHandler(this.mouse_enter);
            this.E5.MouseLeave += new System.EventHandler(this.mouse_leave);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(203, 206);
            this.Controls.Add(this.E3);
            this.Controls.Add(this.D3);
            this.Controls.Add(this.C3);
            this.Controls.Add(this.E2);
            this.Controls.Add(this.D2);
            this.Controls.Add(this.C2);
            this.Controls.Add(this.E5);
            this.Controls.Add(this.D5);
            this.Controls.Add(this.E4);
            this.Controls.Add(this.C5);
            this.Controls.Add(this.D4);
            this.Controls.Add(this.C4);
            this.Controls.Add(this.E1);
            this.Controls.Add(this.D1);
            this.Controls.Add(this.C1);
            this.Controls.Add(this.B3);
            this.Controls.Add(this.B5);
            this.Controls.Add(this.B2);
            this.Controls.Add(this.B4);
            this.Controls.Add(this.A5);
            this.Controls.Add(this.B1);
            this.Controls.Add(this.A4);
            this.Controls.Add(this.A3);
            this.Controls.Add(this.A2);
            this.Controls.Add(this.A1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Хрестики-нолики";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem играToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newGameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Button A1;
        private System.Windows.Forms.Button A2;
        private System.Windows.Forms.Button A3;
        private System.Windows.Forms.Button B1;
        private System.Windows.Forms.Button B2;
        private System.Windows.Forms.Button B3;
        private System.Windows.Forms.Button C1;
        private System.Windows.Forms.Button C2;
        private System.Windows.Forms.Button C3;
        private System.Windows.Forms.ToolStripMenuItem режимToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem WithPCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem WithFriendToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem полеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem х4ToolStripMenuItem;
        private System.Windows.Forms.Button A4;
        private System.Windows.Forms.Button B4;
        private System.Windows.Forms.Button C4;
        private System.Windows.Forms.Button D1;
        private System.Windows.Forms.Button D4;
        private System.Windows.Forms.Button D2;
        private System.Windows.Forms.Button D3;
        private System.Windows.Forms.ToolStripMenuItem x3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem x5ToolStripMenuItem;
        private System.Windows.Forms.Button E1;
        private System.Windows.Forms.Button E4;
        private System.Windows.Forms.Button E2;
        private System.Windows.Forms.Button E3;
        private System.Windows.Forms.Button A5;
        private System.Windows.Forms.Button B5;
        private System.Windows.Forms.Button C5;
        private System.Windows.Forms.Button D5;
        private System.Windows.Forms.Button E5;
    }
}

