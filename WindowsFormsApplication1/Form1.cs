﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        bool turn = true;
        int turn_count = 0;
        bool agan_computer = false;
        int i = 0;
        int r = 0; // режим (3х3/4х4/5х5)


        public Form1()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_click(object sender, EventArgs e)
        {
            if (r == 0)
                MessageBox.Show("Оберіть поле для гри! 3х3/4х4/5х5!");

            Button b = (Button)sender;
            if (turn)
                b.Text = "X";
            else
                b.Text = "O";

            turn = !turn;
            b.Enabled = false;
            turn_count++;
            CheckForWinner();
            
            if ((!turn) && (agan_computer))
            {
                computer_move();
            }
            
        }
        private void computer_move()
        {
            Button move = null;

            if (r == 3)
            {
                move = look_win_or_block_3_3("0");

                if (move == null)
                {
                    move = look_win_or_block_3_3("X");
                    if (move == null)
                    {

                        move = look_corners_3_3();
                        if (move == null)
                        {
                            move = look_open_space();
                        }
                    }
                }
            }

            if (r == 4)
            {
                move = look_win_or_block_4_4("0");

                if (move == null)
                {
                    move = look_win_or_block_4_4("X");
                    if (move == null)
                    {

                        move = look_corners_4_4();
                        if (move == null)
                        {
                            move = look_open_space();
                        }
                    }
                }
            }

            if (r == 5)
            {
                move = look_win_or_block_5_5("0");

                if (move == null)
                {
                    move = look_win_or_block_5_5("X");
                    if (move == null)
                    {

                        move = look_corners_5_5();
                        if (move == null)
                        {
                            move = look_open_space();
                        }
                    }
                }
            }
            try
            {
                move.PerformClick();
                i++;
            }
            catch { }

        }
        private void close_other_4x4()
        {
            A5.Enabled = false;
            B5.Enabled = false;
            C5.Enabled = false;
            D5.Enabled = false;

            E1.Enabled = false;
            E2.Enabled = false;
            E3.Enabled = false;
            E4.Enabled = false;
            E5.Enabled = false;
            
        }
        private void close_other_3x3()
        {
            A4.Enabled = false;
            B4.Enabled = false;
            C4.Enabled = false;

            A5.Enabled = false;
            B5.Enabled = false;
            C5.Enabled = false;


            E1.Enabled = false;
            E2.Enabled = false;
            E3.Enabled = false;
            E4.Enabled = false;
            E5.Enabled = false;
            D1.Enabled = false;
            D2.Enabled = false;
            D3.Enabled = false;
            D4.Enabled = false;
            D5.Enabled = false;
        }
        private Button look_open_space()
        {
            if (r == 4)
                close_other_4x4();
            else if (r == 3)
                close_other_3x3();
            
            

            Button b = null;
            foreach (Control c in Controls)
            {
                b = c as Button;
                if (b != null)
                {
                    if (b.Enabled == true)
                    {
                        if (b.Text == "")
                            return b;
                    }
                }
            }

            return null;
        }

        private Button look_corners_5_5()
        {

            if (A1.Text == "O")
            {
                if (A5.Text == "")
                    return A5;
                if (E5.Text == "")
                    return E5;
                if (E1.Text == "")
                    return E1;
            }

            if (A5.Text == "O")
            {
                if (A1.Text == "")
                    return A1;
                if (E5.Text == "")
                    return E5;
                if (E1.Text == "")
                    return E1;
            }

            if (E5.Text == "O")
            {
                if (A1.Text == "")
                    return A1;
                if (A5.Text == "")
                    return A5;
                if (E1.Text == "")
                    return E1;
            }

            if (E1.Text == "O")
            {
                if (A1.Text == "")
                    return A1;
                if (A5.Text == "")
                    return A5;
                if (E5.Text == "")
                    return E5;
            }


            if (A1.Text == "")
                return A1;
            if (A5.Text == "")
                return A5;
            if (E1.Text == "")
                return E1;
            if (E5.Text == "")
                return E5;

            return null;
        }
        private Button look_corners_4_4()
        {
            if (A1.Text == "O")
            {
                if (A4.Text == "")
                    return A4;
                if (D4.Text == "")
                    return D4;
                if (D1.Text == "")
                    return D1;
            }

            if (A4.Text == "O")
            {
                if (A1.Text == "")
                    return A1;
                if (D4.Text == "")
                    return D4;
                if (D1.Text == "")
                    return D1;
            }

            if (D4.Text == "O")
            {
                if (A1.Text == "")
                    return A1;
                if (A4.Text == "")
                    return A4;
                if (D1.Text == "")
                    return D1;
            }

            if (D1.Text == "O")
            {
                if (A1.Text == "")
                    return A1;
                if (A4.Text == "")
                    return A4;
                if (D4.Text == "")
                    return D4;
            }


            if (A1.Text == "")
                return A1;
            if (A4.Text == "")
                return A4;
            if (D1.Text == "")
                return D1;
            if (D4.Text == "")
                return D4;

            return null;
        }
        private Button look_corners_3_3()
        {
            
                if (A1.Text == "O")
                {
                    if (A3.Text == "")
                        return A3;
                    if (C3.Text == "")
                        return C3;
                    if (C1.Text == "")
                        return C1;
                }

                if (A3.Text == "O")
                {
                    if (A1.Text == "")
                        return A1;
                    if (C3.Text == "")
                        return C3;
                    if (C1.Text == "")
                        return C1;
                }

                if (C3.Text == "O")
                {
                    if (A1.Text == "")
                        return A3;
                    if (A3.Text == "")
                        return A3;
                    if (C1.Text == "")
                        return C1;
                }

                if (C1.Text == "O")
                {
                    if (A1.Text == "")
                        return A3;
                    if (A3.Text == "")
                        return A3;
                    if (C3.Text == "")
                        return C3;
                }


                if (A1.Text == "")
                    return A1;
                if (A3.Text == "")
                    return A3;
                if (C1.Text == "")
                    return C1;
                if (C3.Text == "")
                    return C3;

                return null;
            
        }
        private Button look_win_or_block_5_5(string PC)
        {
            //ГОРИЗОНТАЛЬНа ПЕРЕВІРКА
            if ((A1.Text == PC) && (A2.Text == PC) && (A3.Text == PC) && (A4.Text == PC) && (A5.Text == ""))
                return A5;
            if ((A1.Text == PC) && (A2.Text == PC) && (A3.Text == PC) && (A4.Text == "") && (A5.Text == PC))
                return A4;
            if ((A1.Text == PC) && (A2.Text == PC) && (A3.Text == "") && (A4.Text == PC) && (A5.Text == PC))
                return A3;
            if ((A1.Text == PC) && (A2.Text == "") && (A3.Text == PC) && (A4.Text == PC) && (A5.Text == PC))
                return A2;
            if ((A1.Text == "") && (A2.Text == PC) && (A3.Text == PC) && (A4.Text == PC) && (A5.Text == PC))
                return A1;

            if ((B1.Text == PC) && (B2.Text == PC) && (B3.Text == PC) && (B4.Text == PC) && (B5.Text == ""))
                return B5;
            if ((B1.Text == PC) && (B2.Text == PC) && (B3.Text == PC) && (B4.Text == "") && (B5.Text == PC))
                return B4;
            if ((B1.Text == PC) && (B2.Text == PC) && (B3.Text == "") && (B4.Text == PC) && (B5.Text == PC))
                return B3;
            if ((B1.Text == PC) && (B2.Text == "") && (B3.Text == PC) && (B4.Text == PC) && (B5.Text == PC))
                return B2;
            if ((B1.Text == "") && (B2.Text == PC) && (B3.Text == PC) && (B4.Text == PC) && (B5.Text == PC))
                return B1;

            if ((C1.Text == PC) && (C2.Text == PC) && (C3.Text == PC) && (C4.Text == PC) && (C5.Text == ""))
                return C5;
            if ((C1.Text == PC) && (C2.Text == PC) && (C3.Text == PC) && (C4.Text == "") && (C5.Text == PC))
                return C4;
            if ((C1.Text == PC) && (C2.Text == PC) && (C3.Text == "") && (C4.Text == PC) && (C5.Text == PC))
                return C3;
            if ((C1.Text == PC) && (C2.Text == "") && (C3.Text == PC) && (C4.Text == PC) && (C5.Text == PC))
                return C2;
            if ((C1.Text == "") && (C2.Text == PC) && (C3.Text == PC) && (C4.Text == PC) && (C5.Text == PC))
                return C1;

            if ((D1.Text == PC) && (D2.Text == PC) && (D3.Text == PC) && (D4.Text == PC) && (D5.Text == ""))
                return D5;
            if ((D1.Text == PC) && (D2.Text == PC) && (D3.Text == PC) && (D4.Text == "") && (D5.Text == PC))
                return D4;
            if ((D1.Text == PC) && (D2.Text == PC) && (D3.Text == "") && (D4.Text == PC) && (D5.Text == PC))
                return D3;
            if ((D1.Text == PC) && (D2.Text == "") && (D3.Text == PC) && (D4.Text == PC) && (D5.Text == PC))
                return D2;
            if ((D1.Text == "") && (D2.Text == PC) && (D3.Text == PC) && (D4.Text == PC) && (D5.Text == PC))
                return D1;

            if ((E1.Text == PC) && (E2.Text == PC) && (E3.Text == PC) && (E4.Text == PC) && (E5.Text == ""))
                return E5;
            if ((E1.Text == PC) && (E2.Text == PC) && (E3.Text == PC) && (E4.Text == "") && (E5.Text == PC))
                return E4;
            if ((E1.Text == PC) && (E2.Text == PC) && (E3.Text == "") && (E4.Text == PC) && (E5.Text == PC))
                return E3;
            if ((E1.Text == PC) && (E2.Text == "") && (E3.Text == PC) && (E4.Text == PC) && (E5.Text == PC))
                return E2;
            if ((E1.Text == "") && (E2.Text == PC) && (E3.Text == PC) && (E4.Text == PC) && (E5.Text == PC))
                return E1;
        
            //ВЕРТИКАЛЬНА ПЕРЕВІРКА
            if ((A1.Text == PC) && (B1.Text == PC) && (C1.Text == PC) && (D1.Text == PC) && (E1.Text == ""))
                return E1;
            if ((A1.Text == PC) && (B1.Text == PC) && (C1.Text == PC) && (D1.Text == "") && (E1.Text == PC))
                return D1;
            if ((A1.Text == PC) && (B1.Text == PC) && (C1.Text == "") && (D1.Text == PC) && (E1.Text == PC))
                return C1;
            if ((A1.Text == PC) && (B1.Text == "") && (C1.Text == PC) && (D1.Text == PC) && (E1.Text == PC))
                return B1;
            if ((A1.Text == "") && (B1.Text == PC) && (C1.Text == PC) && (D1.Text == PC) && (E1.Text == PC))
                return A1;

            if ((A2.Text == PC) && (B2.Text == PC) && (C2.Text == PC) && (D2.Text == PC) && (E2.Text == ""))
                return E2;
            if ((A2.Text == PC) && (B2.Text == PC) && (C2.Text == PC) && (D2.Text == "") && (E2.Text == PC))
                return D2;
            if ((A2.Text == PC) && (B2.Text == PC) && (C2.Text == "") && (D2.Text == PC) && (E2.Text == PC))
                return C2;
            if ((A2.Text == PC) && (B2.Text == "") && (C2.Text == PC) && (D2.Text == PC) && (E2.Text == PC))
                return B2;
            if ((A2.Text == "") && (B2.Text == PC) && (C2.Text == PC) && (D2.Text == PC) && (E2.Text == PC))
                return A2;

            if ((A3.Text == PC) && (B3.Text == PC) && (C3.Text == PC) && (D3.Text == PC) && (E3.Text == ""))
                return E3;
            if ((A3.Text == PC) && (B3.Text == PC) && (C3.Text == PC) && (D3.Text == "") && (E3.Text == PC))
                return D3;
            if ((A3.Text == PC) && (B3.Text == PC) && (C3.Text == "") && (D3.Text == PC) && (E3.Text == PC))
                return C3;
            if ((A3.Text == PC) && (B3.Text == "") && (C3.Text == PC) && (D3.Text == PC) && (E3.Text == PC))
                return B3;
            if ((A3.Text == "") && (B3.Text == PC) && (C3.Text == PC) && (D3.Text == PC) && (E3.Text == PC))
                return A3;

            if ((A4.Text == PC) && (B4.Text == PC) && (C4.Text == PC) && (D4.Text == PC) && (E4.Text == ""))
                return E4;
            if ((A4.Text == PC) && (B4.Text == PC) && (C4.Text == PC) && (D4.Text == "") && (E4.Text == PC))
                return D4;
            if ((A4.Text == PC) && (B4.Text == PC) && (C4.Text == "") && (D4.Text == PC) && (E4.Text == PC))
                return C4;
            if ((A4.Text == PC) && (B4.Text == "") && (C4.Text == PC) && (D4.Text == PC) && (E4.Text == PC))
                return B4;
            if ((A4.Text == "") && (B4.Text == PC) && (C4.Text == PC) && (D4.Text == PC) && (E4.Text == PC))
                return A4;

            if ((A5.Text == PC) && (B5.Text == PC) && (C5.Text == PC) && (D5.Text == PC) && (E4.Text == ""))
                return E5;
            if ((A5.Text == PC) && (B5.Text == PC) && (C5.Text == PC) && (D5.Text == "") && (E4.Text == PC))
                return D5;
            if ((A5.Text == PC) && (B5.Text == PC) && (C5.Text == "") && (D5.Text == PC) && (E4.Text == PC))
                return C5;
            if ((A5.Text == PC) && (B5.Text == "") && (C5.Text == PC) && (D5.Text == PC) && (E4.Text == PC))
                return B5;
            if ((A5.Text == "") && (B5.Text == PC) && (C5.Text == PC) && (D5.Text == PC) && (E4.Text == PC))
                return A5;

            //ДІАГОНАЛЬ
            if ((A1.Text == PC) && (B2.Text == PC) && (C3.Text == PC) && (D4.Text == PC) && (E5.Text == ""))
                return E5;
            if ((A1.Text == PC) && (B2.Text == PC) && (C3.Text == PC) && (D4.Text == "") && (E5.Text == PC))
                return D4;
            if ((A1.Text == PC) && (B2.Text == PC) && (C3.Text == "") && (D4.Text == PC) && (E5.Text == PC))
                return C3;
            if ((A1.Text == PC) && (B2.Text == "") && (C3.Text == PC) && (D4.Text == PC) && (E5.Text == PC))
                return B2;
            if ((A1.Text == "") && (B2.Text == PC) && (C3.Text == PC) && (D4.Text == PC) && (E5.Text == PC))
                return A1;

            if ((A5.Text == PC) && (B4.Text == PC) && (C3.Text == PC) && (D2.Text == PC) && (E1.Text == ""))
                return E1;
            if ((A5.Text == PC) && (B4.Text == PC) && (C3.Text == PC) && (D2.Text == "") && (E1.Text == PC))
                return D2;
            if ((A5.Text == PC) && (B4.Text == PC) && (C3.Text == "") && (D2.Text == PC) && (E1.Text == PC))
                return C3;
            if ((A5.Text == PC) && (B4.Text == "") && (C3.Text == PC) && (D2.Text == PC) && (E1.Text == PC))
                return B4;
            if ((A5.Text == "") && (B4.Text == PC) && (C3.Text == PC) && (D2.Text == PC) && (E1.Text == PC))
                return A5;

            return null;
        }

        private Button look_win_or_block_4_4(string PC)
        {
            //ГОРИЗОНТАЛЬНА ПЕРЕВІРКА
            if ((A1.Text == "") && (A2.Text == PC) && (A3.Text == PC) && (A4.Text == PC))
                return A1;
            if ((A1.Text == PC) && (A2.Text == "") && (A3.Text == PC) && (A4.Text == PC))
                return A2;
            if ((A1.Text == PC) && (A2.Text == PC) && (A3.Text == "") && (A4.Text == PC))
                return A3;
            if ((A1.Text == PC) && (A2.Text == PC) && (A3.Text == PC) && (A4.Text == ""))
                return A4;

            if ((B1.Text == "") && (B2.Text == PC) && (B3.Text == PC) && (B4.Text == PC))
                return B1;
            if ((B1.Text == PC) && (B2.Text == "") && (B3.Text == PC) && (B4.Text == PC))
                return B2;
            if ((B1.Text == PC) && (B2.Text == PC) && (B3.Text == "") && (B4.Text == PC))
                return B3;
            if ((B1.Text == PC) && (B3.Text == PC) && (B2.Text == PC) && (B4.Text == ""))
                return B4;

            if ((C1.Text == "") && (C2.Text == PC) && (C3.Text == PC) && (C4.Text == PC))
                return C1;
            if ((C1.Text == PC) && (C2.Text == "") && (C3.Text == PC) && (C4.Text == PC))
                return C2;
            if ((C1.Text == PC) && (C2.Text == PC) && (C3.Text == "") && (C4.Text == PC))
                return C3;
            if ((C1.Text == PC) && (C3.Text == PC) && (C2.Text == PC) && (C4.Text == ""))
                return C4;

            if ((D1.Text == "") && (D2.Text == PC) && (D3.Text == PC) && (D4.Text == PC))
                return D1;
            if ((D1.Text == PC) && (D2.Text == PC) && (D4.Text == PC) && (D3.Text == ""))
                return D3;
            if ((D1.Text == PC) && (D2.Text == "") && (D3.Text == PC) && (D4.Text == PC))
                return D2;
            if ((D1.Text == PC) && (D3.Text == PC) && (D2.Text == PC) && (D4.Text == ""))
                return D4;

            //ВЕРТИКАЛЬНА ПЕРЕВІРКА
            if ((A1.Text == PC) && (B1.Text == PC) && (D1.Text == PC) && (C1.Text == ""))
                return C1;
            if ((A1.Text == "") && (B1.Text == PC) && (C1.Text == PC) && (D1.Text == PC))
                return A1;
            if ((A1.Text == PC) && (B1.Text == "") && (C1.Text == PC) && (D1.Text == PC))
                return B1;
            if ((A1.Text == PC) && (B1.Text == PC) && (C1.Text == PC) && (D1.Text == ""))
                return D1;

            if ((A2.Text == "") && (B2.Text == PC) && (C2.Text == PC) && (D2.Text == PC))
                return A2;
            if ((A2.Text == PC) && (B2.Text == PC) && (C2.Text == "") && (D2.Text == PC))
                return C2;
            if ((A2.Text == PC) && (B2.Text == "") && (C2.Text == PC) && (D2.Text == PC))
                return B2;
            if ((A2.Text == PC) && (C2.Text == PC) && (B2.Text == PC) && (D2.Text == ""))
                return D2;

            if ((A3.Text == "") && (B2.Text == PC) && (C3.Text == PC) && (D3.Text == PC))
                return A3;
            if ((A3.Text == PC) && (B3.Text == "") && (C3.Text == PC) && (D3.Text == PC))
                return B3;
            if ((A3.Text == PC) && (B3.Text == PC) && (C3.Text == "") && (D3.Text == PC))
                return C3;
            if ((A3.Text == PC) && (C3.Text == PC) && (B3.Text == PC) && (D3.Text == ""))
                return D3;

            if ((A4.Text == "") && (B4.Text == PC) && (C4.Text == PC) && (D4.Text == PC))
                return A4;
            if ((A4.Text == PC) && (B4.Text == "") && (C4.Text == PC) && (D4.Text == PC))
                return B4;
            if ((A4.Text == PC) && (B4.Text == PC) && (C4.Text == "") && (D4.Text == PC))
                return C4;
            if ((A4.Text == PC) && (C4.Text == PC) && (B4.Text == PC) && (D4.Text == ""))
                return D4;

            //ДІАГОНАЛЬ
            if ((A1.Text == "") && (B2.Text == PC) && (C3.Text == PC) && (D4.Text == PC))
                return A1;
            if ((A1.Text == PC) && (B2.Text == "") && (C3.Text == PC) && (D4.Text == PC))
                return B2;
            if ((A1.Text == PC) && (B2.Text == PC) && (C3.Text == "") && (D4.Text == PC))
                return C3;
            if ((A1.Text == PC) && (C3.Text == PC) && (B2.Text == PC) && (D4.Text == ""))
                return D4;

            if ((A4.Text == "") && (B3.Text == PC) && (C2.Text == PC) && (D1.Text == PC))
                return A4;
            if ((A4.Text == PC) && (B3.Text == "") && (C2.Text == PC) && (D1.Text == PC))
                return B3;
            if ((A4.Text == PC) && (B3.Text == PC) && (C2.Text == "") && (D1.Text == PC))
                return C2;
            if ((A4.Text == PC) && (C2.Text == PC) && (B3.Text == PC) && (D1.Text == ""))
                return D1;

            return null;
        }
        private Button look_win_or_block_3_3(string PC)
        {
            //ГОРИЗОНТАЛЬНА ПЕРЕВІРКА
            if ((A2.Text == PC) && (A3.Text == PC) && (A1.Text == ""))
                return A1;
            if ((A1.Text == PC) && (A3.Text == PC) && (A2.Text == ""))
                return A2;
            if ((A1.Text == PC) && (A2.Text == PC) && (A3.Text == ""))
                return A3;

            if ((B2.Text == PC) && (B3.Text == PC) && (B1.Text == ""))
                return B1;
            if ((B1.Text == PC) && (B3.Text == PC) && (B2.Text == ""))
                return B2;
            if ((B1.Text == PC) && (B2.Text == PC) && (B3.Text == ""))
                return B3;

            if ((C2.Text == PC) && (C3.Text == PC) && (C1.Text == ""))
                return C1;
            if ((C1.Text == PC) && (C3.Text == PC) && (C2.Text == ""))
                return C2;
            if ((C1.Text == PC) && (C2.Text == PC) && (C3.Text == ""))
                return C3;

            //ВЕРТИКАЛЬНА ПЕРЕВІРКА
            if ((A1.Text == PC) && (B1.Text == PC) && (C1.Text == ""))
                return C1;
            if ((B1.Text == PC) && (C1.Text == PC) && (A1.Text == ""))
                return A1;
            if ((A1.Text == PC) && (C1.Text == PC) && (B1.Text == ""))
                return B1;

            if ((B2.Text == PC) && (C2.Text == PC) && (A2.Text == ""))
                return A2;
            if ((A2.Text == PC) && (C2.Text == PC) && (B2.Text == ""))
                return B2;
            if ((A2.Text == PC) && (B2.Text == PC) && (C2.Text == ""))
                return C2;

            if ((B2.Text == PC) && (C3.Text == PC) && (A3.Text == ""))
                return A3;
            if ((A3.Text == PC) && (C3.Text == PC) && (B3.Text == ""))
                return B3;
            if ((A3.Text == PC) && (B3.Text == PC) && (C3.Text == ""))
                return C3;

            //ДІАГОНАЛЬ
            if ((B2.Text == PC) && (C3.Text == PC) && (A1.Text == ""))
                return A1;
            if ((A1.Text == PC) && (C3.Text == PC) && (B2.Text == ""))
                return B2;
            if ((A1.Text == PC) && (B2.Text == PC) && (C3.Text == ""))
                return C3;

            if ((A3.Text == PC) && (B2.Text == PC) && (C1.Text == ""))
                return C1;
            if ((B2.Text == PC) && (C1.Text == PC) && (A3.Text == ""))
                return A3;
            if ((A3.Text == PC) && (C1.Text == PC) && (B2.Text == ""))
                return B2;

            return null;
        }

        private void CheckForWinner()
        {
            bool there_is_winner = false;
            if (r == 3)

            {
                //горизонтальна перевірка
                if ((A1.Text == A2.Text) && (A2.Text == A3.Text) && (!A1.Enabled))
                    there_is_winner = true;
                else if ((B1.Text == B2.Text) && (B2.Text == B3.Text) && (!B1.Enabled))
                    there_is_winner = true;
                else if ((C1.Text == C2.Text) && (C2.Text == C3.Text) && (!C1.Enabled))
                    there_is_winner = true;


                //вертикальна перевірка
                else if ((A1.Text == B1.Text) && (B1.Text == C1.Text) && (!A1.Enabled))
                    there_is_winner = true;
                else if ((A2.Text == B2.Text) && (B2.Text == C2.Text) && (!A2.Enabled))
                    there_is_winner = true;
                else if ((A3.Text == B3.Text) && (B3.Text == C3.Text) && (!A3.Enabled))
                    there_is_winner = true;


                //діагональна перевірка
                else if ((A1.Text == B2.Text) && (B2.Text == C3.Text) && (!A1.Enabled))
                    there_is_winner = true;
                else if ((A3.Text == B2.Text) && (B2.Text == C1.Text) && (!C1.Enabled))
                    there_is_winner = true;
            };


            if (r == 4)
            {
                //горизонтальная перевірка
                if ((A1.Text == A2.Text) && (A2.Text == A3.Text) && (A3.Text == A4.Text) && (!A1.Enabled))
                    there_is_winner = true;
                else if ((B1.Text == B2.Text) && (B2.Text == B3.Text) && (B3.Text == B4.Text) && (!B1.Enabled))
                    there_is_winner = true;
                else if ((C1.Text == C2.Text) && (C2.Text == C3.Text) && (C3.Text == C4.Text) && (!C1.Enabled))
                    there_is_winner = true;
                else if ((D1.Text == D2.Text) && (D2.Text == D3.Text) && (D3.Text == D4.Text) && (!D1.Enabled))
                    there_is_winner = true;


                //вертикальна перевірка
                else if ((A1.Text == B1.Text) && (B1.Text == C1.Text) && (C1.Text == D1.Text) && (!A1.Enabled))
                    there_is_winner = true;
                else if ((A2.Text == B2.Text) && (B2.Text == C2.Text) && (C2.Text == D2.Text) && (!A2.Enabled))
                    there_is_winner = true;
                else if ((A3.Text == B3.Text) && (B3.Text == C3.Text) && (C3.Text == D3.Text) && (!A3.Enabled))
                    there_is_winner = true;
                else if ((A3.Text == B3.Text) && (B3.Text == C3.Text) && (C4.Text == D4.Text) && (!A3.Enabled))
                    there_is_winner = true;


                //діагональна перевірка
                else if ((A1.Text == B2.Text) && (B2.Text == C3.Text) && (C3.Text == D4.Text) && (!A1.Enabled))
                    there_is_winner = true;
                else if ((A4.Text == B3.Text) && (B3.Text == C2.Text) && (C2.Text == D1.Text) && (!D1.Enabled))
                    there_is_winner = true;
            };



            if (r == 5)
            {
                //горизонтальна перевірка
                if ((A1.Text == A2.Text) && (A2.Text == A3.Text) && (A3.Text == A4.Text) && (A4.Text == A5.Text) && (!A1.Enabled))
                    there_is_winner = true;
                else if ((B1.Text == B2.Text) && (B2.Text == B3.Text) && (B3.Text == B4.Text) && (B4.Text == B5.Text) && (!B1.Enabled))
                    there_is_winner = true;
                else if ((C1.Text == C2.Text) && (C2.Text == C3.Text) && (C3.Text == C4.Text) && (C4.Text == C5.Text) && (!C1.Enabled))
                    there_is_winner = true;
                else if ((D1.Text == D2.Text) && (D2.Text == D3.Text) && (D3.Text == D4.Text) && (D4.Text == D5.Text) && (!D1.Enabled))
                    there_is_winner = true;
                else if ((E1.Text == E2.Text) && (E2.Text == E3.Text) && (E3.Text == E4.Text) && (E4.Text == A5.Text) && (!E1.Enabled))
                    there_is_winner = true;


                //вертикальна перевірка
                else if ((A1.Text == B1.Text) && (B1.Text == C1.Text) && (C1.Text == D1.Text) && (D1.Text == E1.Text) && (!A1.Enabled))
                    there_is_winner = true;
                else if ((A2.Text == B2.Text) && (B2.Text == C2.Text) && (C2.Text == D2.Text) && (D2.Text == E2.Text) && (!A2.Enabled))
                    there_is_winner = true;
                else if ((A3.Text == B3.Text) && (B3.Text == C3.Text) && (C3.Text == D3.Text) && (D3.Text == E3.Text) && (!A3.Enabled))
                    there_is_winner = true;
                else if ((A4.Text == B4.Text) && (B4.Text == C4.Text) && (C4.Text == D4.Text) && (D4.Text == E4.Text) && (!A4.Enabled))
                    there_is_winner = true;
                else if ((A5.Text == B5.Text) && (B5.Text == C5.Text) && (C5.Text == D5.Text) && (D5.Text == E5.Text) && (!A5.Enabled))
                    there_is_winner = true;
               


                //діагональна перевірка
                else if ((A1.Text == B2.Text) && (B2.Text == C3.Text) && (C3.Text == D4.Text) && (D4.Text == E5.Text) && (!A1.Enabled))
                    there_is_winner = true;
                else if ((A5.Text == B4.Text) && (B4.Text == C3.Text) && (C3.Text == D2.Text) && (D2.Text == E1.Text) && (!E1.Enabled))
                    there_is_winner = true;
            }
            

            if (there_is_winner)
            {
                disableButtons();
                String winner = "";
                if (turn)
                    winner = "O";
                else
                    winner = "X";

                MessageBox.Show(winner + "Перемогли!");
            }
            else
            {
                if (r == 3)
                {
                    if (turn_count == 9)
                        MessageBox.Show("Нічия!", "Кінець гри!");
                }
                else if (r == 4)
                {
                    if (turn_count == 16)
                        MessageBox.Show("Нічия!", "Кінець гри!");
                }
                else if (r == 5)
                {
                    if(turn_count==25)
                        MessageBox.Show("Нічия!", "Кінець гри!");
                };
            }
        }

        private void disableButtons()
        {
            try
            {
                foreach (Control c in Controls)
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }
            }
            catch { }
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            turn = true;
            turn_count = 0;

            try
            {
                foreach (Control c in Controls)
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";
                }
            }
            catch { }
        }

        private void mouse_enter(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                if (turn)
                    b.Text = "X";
                else
                    b.Text = "O";
            }
        }

        private void mouse_leave(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                b.Text = "";
            }
        }

        private void WithPCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            agan_computer = true;
            BackColor = Color.Red;
        }

        private void WithFriendToolStripMenuItem_Click(object sender, EventArgs e)
        {
            agan_computer = false;
            BackColor = Color.Blue;
        }

        private void x3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Width = 207;
            Height = 233;
            r = 3;

        }

        private void х4ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Width = 274;
            Height = 294;
            r = 4;

            
        }

        private void x5ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Width = 345;
            Height = 352;
            r = 5;
        }

        
    }
}
